
import org.apache.http.HttpHost
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.electoralist.locations.controllers.{DistrictController, HealthController, SearchController, StateController}
import org.electoralist.locations.services.{KeywordSearchService, LocationSearchSevice, NamedDistrictSearchService, SearchService, StateSearchService}
import org.opensearch.client.{RestClient, RestClientBuilder}
import org.scalatra._

import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    println("Bootstrapping...")
    val restClientBuilder = RestClient.builder(
      new HttpHost(
        if (System.getenv("ES_HOST") != null)
          System.getenv("ES_HOST")
        else "localhost",
        if (System.getenv("ES_PORT") != null)
          Integer.parseInt(System.getenv("ES_PORT"))
        else 9200,
        "http")).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
      override def customizeHttpClient(httpClientBuilder: HttpAsyncClientBuilder): HttpAsyncClientBuilder = httpClientBuilder //.setDefaultCredentialsProvider(credentialsProvider)
    })

    context.mount(new HealthController, "/api/locations/health")
    context.mount(new StateController, "/api/locations/state")
    context.mount(new DistrictController, "/api/locations/district")

    context.mount(
      new SearchController(
        new SearchService(
          new NamedDistrictSearchService,
          new StateSearchService,
          new LocationSearchSevice(restClientBuilder),
          new KeywordSearchService
        )
      ), "/api/locations/search/v1")
    println("Bootstrapping complete.")

  }
}