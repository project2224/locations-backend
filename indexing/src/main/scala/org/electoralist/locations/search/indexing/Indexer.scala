package org.electoralist.locations.search.indexing

import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.HttpHost
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.electoralist.JsonUtil
import org.electoralist.locations.search.indexing.clients.{LocationsRestClient, LocationsRestClientConfig}
import org.electoralist.locations.search.indexing.search.models.{BallotAccess, DistrictRace, SearchResultType, WriteInAccess}
import org.opensearch.action.index.IndexRequest
import org.opensearch.client.{RequestOptions, RestClient, RestClientBuilder, RestHighLevelClient}
import org.opensearch.client.indices.CreateIndexRequest
import org.opensearch.common.xcontent.XContentType

import scala.io.{Codec, Source}

object IndexerAppp extends App {
  val indexer: Indexer = new Indexer()
  indexer.createIndex()
}

class Indexer {
  def createIndex() = {
    val restClientBuilder = RestClient.builder(
      new HttpHost(
        if (System.getenv("ES_HOST") != null)
          System.getenv("ES_HOST")
        else "localhost",
        if (System.getenv("ES_PORT") != null)
          Integer.parseInt(System.getenv("ES_PORT"))
        else 9200,
        "http")).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
      override def customizeHttpClient(httpClientBuilder: HttpAsyncClientBuilder): HttpAsyncClientBuilder = httpClientBuilder //.setDefaultCredentialsProvider(credentialsProvider)
    })

    val client = new RestHighLevelClient(restClientBuilder)

    import org.opensearch.common.settings.Settings
    val createIndexRequest = new CreateIndexRequest("custom-index-2")

    createIndexRequest.settings(Settings.builder.put //Specify in the settings how many shards you want in the index.
    ("index.number_of_shards", 1).put("index.number_of_replicas", 1))


    val districtMappingsUrl = this.getClass.getClassLoader.getResource("mappings/district-race.json")
    val districtMappingJsonSource = Source.fromURL(districtMappingsUrl)(Codec.UTF8)
    val distringMappingJsonMapper = new ObjectMapper()
    val distringMappingJsonMapperTypeRef = new TypeReference[java.util.HashMap[String, Object]]() {}
    val propertiesMapping = distringMappingJsonMapper.readValue(districtMappingJsonSource.reader(), distringMappingJsonMapperTypeRef)

    val mapping: java.util.HashMap[String, Object] = new java.util.HashMap[String, Object]()

    mapping.put("properties", propertiesMapping)
    createIndexRequest.mapping(mapping)
    val createIndexResponse = client.indices.create(createIndexRequest, RequestOptions.DEFAULT)


    val lrc = new LocationsRestClient(new LocationsRestClientConfig("http://localhost:8082/api/locations"))
    val statesDistricts = lrc.getDistrictsByStateCode("CA")
    statesDistricts.foreach(restDistrict => {
      println(s"indexing district ${restDistrict}")

      val outlineMap = new OutlineReader().makeOutline(f"/Users/stevejacowski/Projects/maps-generators/output/${restDistrict.district.state.code}-${restDistrict.district.districtNumber}.json")

      val districtNumber = s"${restDistrict.district.districtNumber}".reverse.padTo(2, "0").reverse

      val resultName = if (statesDistricts.length > 1) s"${restDistrict.district.state.name}'s ${englishOrdinal(restDistrict.district.districtNumber)} district" else s"${restDistrict.district.state.name}'s at-large district"
      val districtRace = DistrictRace(
        restDistrict.id,
        Some(outlineMap),
        "DISTRICT_RESULT",
        if (statesDistricts.length > 1)
          List(s"${restDistrict.district.state.code}-${districtNumber}")
        else
          List(s"${restDistrict.district.state.code}-AL", s"${restDistrict.district.state.code}-${districtNumber}"),
        restDistrict.district.state.code,
        Integer.parseInt(restDistrict.date.substring(0, 4)),
        restDistrict.date,
        restDistrict.comments,
        restDistrict.extendedComments,
        resultName,
        restDistrict.ballotAccesses.map(BallotAccess(_, restDistrict.district.state)),
        WriteInAccess(restDistrict.writeInAccess)
      )

      val request = new IndexRequest("custom-index-2") //Add a document to the custom-index we created.
      request.id(s"${restDistrict.id}") //Assign an ID to the document.
      val docuBytes = JsonUtil.toJson(districtRace)
      request.source(docuBytes, XContentType.JSON)

      val indexResponse = client.index(request, RequestOptions.DEFAULT)

    }
    )


  }

  def englishOrdinal(cardinal: Int): String = {
    if (cardinal >= 10 && cardinal < 20) {
      s"${cardinal}th"
    } else {
      if (s"${cardinal}".endsWith("1")) {
        s"${cardinal}st"
      } else if (s"${cardinal}".endsWith("2")) {
        s"${cardinal}nd"
      } else if (s"${cardinal}".endsWith("3")) {
        s"${cardinal}rd"
      } else {
        s"${cardinal}th"
      }
    }
  }


}

