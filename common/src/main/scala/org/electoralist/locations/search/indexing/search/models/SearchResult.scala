package org.electoralist.locations.search.indexing.search.models

import org.electoralist.locations.search.indexing.search.models.SearchResultType.SearchResultType

object SearchResultType extends Enumeration {
  type SearchResultType = Value

  val STATE_RESULT: SearchResultType = Value("STATE_RESULT")
  val DISTRICT_RESULT: SearchResultType = Value("DISTRICT_RESULT")
}

object SearchResultsType extends Enumeration {
  type SearchResultsType = Value

  val STATE_RESULTS: SearchResultsType = Value("STATE_RESULTS")
  val DISTRICT_RESULTS: SearchResultsType = Value("DISTRICT_RESULTS")
  val MIXED_RESULTS: SearchResultsType = Value("MIXED_RESULTS")
}

case class SearchResults(results: Seq[SearchResult], resultsType: SearchResultsType.SearchResultsType)

trait SearchResult {
  def resultType: String
}

