package org.electoralist.locations.search.indexing.rest.models

case class DistrictRace(
                         id: Int,
                         district: District,
                         date: String,
                         special: Boolean,
                         ballotAccesses: List[BallotAccess],
                         writeInAccess: WriteInAccess,
                         comments: Option[String],
                         extendedComments: Option[String]
                       )

case class District(
                   id: Int,
                   districtNumber: Int,
                   state: State
                   )

case class DistrictList(
                       results: List[District]
                       )