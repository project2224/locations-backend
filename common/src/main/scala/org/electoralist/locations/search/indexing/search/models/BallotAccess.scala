package org.electoralist.locations.search.indexing.search.models

import org.electoralist.locations.search.indexing.rest.models.{BallotAccess => RestBallotAccess, State => RestState}

case class BallotAccess(filingDeadline: String,
                        filingFee: Int,
                        signatureRequirement: Int,
                        applicablePartyIds: List[Int],
                        appliesToUnlistedParties: Boolean,
                        comments: Option[String],
                        extendedComments: Option[String]
                       )

object BallotAccess {
  def apply(restBallotAccess: RestBallotAccess, restState: RestState): BallotAccess = {
    BallotAccess(
      restBallotAccess.filingDeadline,
      restBallotAccess.filingFee,
      restBallotAccess.signatureRequirement,
      List(),
      !restBallotAccess.recognizedParties,
      restBallotAccess.comments,
      restBallotAccess.extendedComments
    )
  }
}