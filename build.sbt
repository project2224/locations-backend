val ScalatraVersion = "2.8.2"

ThisBuild / scalaVersion := "2.13.7"
ThisBuild / organization := "org.electoralist"

lazy val common = (project in file("common"))
  .settings(
      assembly / assemblyMergeStrategy := {
        case PathList("META-INF", xs @ _*) => MergeStrategy.discard
        case "module-info.class" => MergeStrategy.discard
        case x =>
        MergeStrategy.first
    },
    name := "locations-backend-common",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies ++= Seq(),
  )

lazy val web = (project in file("web")).dependsOn(common)
  .settings(
    assembly / mainClass := Some("org.electoralist.locations.Main"),
    assembly / assemblyMergeStrategy := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case "module-info.class" => MergeStrategy.discard
      case x =>
        MergeStrategy.first
    },
    name := "locations-backend-web",
    version := "0.1.0-SNAPSHOT",
    containerPort := 8081,
    libraryDependencies ++= Seq(
      "org.scalatra" %% "scalatra" % ScalatraVersion,
      "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
      "org.eclipse.jetty" % "jetty-webapp" % "9.4.35.v20201120" % "container;compile",
      "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
      "org.scalatra" %% "scalatra-json" % "2.8.2",
      "org.json4s"   %% "json4s-jackson" % "4.0.1",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.13.1",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1",
      "org.opensearch.client" % "opensearch-rest-high-level-client" % "1.2.4",
      "org.locationtech.spatial4j" % "spatial4j" % "0.8"
    ),
  ).enablePlugins(JettyPlugin)



lazy val indexing = (project in file("indexing")).dependsOn(common)
  .settings(
    assembly / assemblyMergeStrategy := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case "module-info.class" => MergeStrategy.discard
      case x =>
        MergeStrategy.first
    },
    name := "locations-backend-indexing",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
      "org.opensearch.client" % "opensearch-rest-high-level-client" % "1.2.4",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.13.1",
      "com.softwaremill.sttp.client3" %% "core" % "3.4.0",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.1"
    ),
  )
